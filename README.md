# Automate restoring EC2 Volume from the Backup with Python

#### Project Outline

Lets say one of the EC2 instances got corrupted and shutdown, luckily our script runs to take a snapshot of the volume. Now we want our EC2 instance back up and running, we can create a script that will restore it from the volume if the ec2 instances went down.

#### Lets get started

Let’s create a new file called restore-volume.py

![Image1](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image1.png)

Using the instance id for the below

![Image2](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image2.png)

Starting off with the client and resource and then hardcoding the instance_id

![Image3](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image3.png)

Now what we want to do is Get Volumes of EC2 instance and the snapshots of Volume

Can then use the below doc to get the filters of the volume  of the particular instance

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_volumes.html

Can use the below filter attribute

![Image4](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image4.png)

Can then implement it like the below

![Image5](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image5.png)

Can then grab the first element of volumes

![Image6](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image6.png)

Can see it produces the instance

![Image7](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image7.png)

Can see it returns the volume directly attached

![Image8](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image8.png)

Can then includes and filter the describe function for snapshots for the one’s created by ourselves

![Image9](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image9.png)

Can then grab the latest snapshot created

![Image10](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image10.png)

Can then run the results

![Image11](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image11.png)

Next we want to create a volume from our snapshot in which our case is the ‘2023-11-29 11:46:20’ one

Now creating the volume, can use the below documentation

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/create_volume.html

Can then create use the above documentation to create a volume from a snapshot

![Image12](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image12.png)

Can then attach the volume to the ec2 instance

Can then navigating to the below documentation to ec2 instance page

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/instance/index.html

Can then navigate to the page attach volume

![Image13](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image13.png)

Can then using the below

![Image14](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image14.png)

Adding Volume Id as that is what is returned when with the response syntax as seen below

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/create_volume.html

![Image15](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image15.png)

Then using the attached instance information

![Image16](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image16.png)

Can then incorporate the below

![Image17](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image17.png)

Now ready to execute

However upon running we see an error where it will state where the Volume is not available, hence creating a timing issue

![Image18](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image18.png)

Despite it being created

![Image19](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image19.png)

And this is due to a timing issue which is due to the state of the Volume and script is waiting for it to be available 

One way we can go about this is using implementing a while loop


Using the below documentation can then introduce a while loop

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/volume/index.html

![Image20](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image20.png)

We also have something called state

![Image21](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image21.png)

Can then if the state is available can then attach the new volume to the the ec2 instance

![Image22](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image22.png)

Can then execute and when the below is the result

![Image23](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image23.png)

Can see the new Volume has now been created

![Image24](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image24.png)

And can see the instance now has a new volume

![Image25](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image25.png)

Can see the new volumes attached

![Image26](https://gitlab.com/FM1995/automate-restoring-ec2-volume-from-the-backup-with-python/-/raw/main/Images/Image26.png)





